<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: hf57
  Date: 1/02/2019
  Time: 12:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <%--CSS external stylesheet--%>
        <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">

        <script type="text/javascript" src="Resources/jquery-3.3.1.js"></script>
        <script type="text/javascript"></script>

        <title>Browse Media</title>
    </head>
    <body>
        <%@include file="Resources/nav.jsp" %>

        <div id="slide-top" class="container">
            <br><br><br><br>

            <div class="card">
                <div class="card-header"></div>
                <div class="card-body">
                    <div class="card-body">
                        <%--Media Content--%>
                        <div class="card-columns">
                            <%--Images--%>
                            <core:forEach items="${mediaList}" var="url" varStatus="loop">
                                <core:if test="${fn:endsWith(url, '.jpeg') || fn:endsWith(url, '.jpg')}">
                                    <div class="card">
                                        <div class="card-body" align="center">
                                            <img id="${'img'+=loop.index}" width="150" height="150"
                                                 src="<core:out value="${url}"/>">
                                        </div>
                                    </div>
                                </core:if>
                                <%--Videos--%>
                                <core:if test="${fn:endsWith(url, '.mp4')}">
                                    <div class="card">
                                        <div class="card-body" align="center">
                                            <video id="${'vid'+=loop.index}" width="150" height="150" controls>
                                                <source src="<core:out value="${url}"/>" type="video/mp4">
                                            </video>
                                        </div>
                                    </div>
                                </core:if>
                                <%--Audio--%>
                                <core:if test="${fn:endsWith(url, '.mp3')}">
                                    <div class="card">
                                        <div class="card-body" align="center">
                                            <audio id="${'aud'+=loop.index}"  controls style="height: 150px;background-image: url('Resources/Audio_play.png')">
                                                <source src="<core:out value="${url}"/>" type="audio/mp3">
                                            </audio>
                                        </div>
                                    </div>
                                </core:if>
                            </core:forEach>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

