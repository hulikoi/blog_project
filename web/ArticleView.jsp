<%--
  Created by IntelliJ IDEA.
  User: dwc1
  Date: 25/01/2019
  Time: 3:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
    <head>

        <%--CSS external style sheet--%>
        <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">

        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">

        <%--Button (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Rajdhani" rel="stylesheet">
        <%--Title (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <%--Date/Author (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">


        <title>Article view</title>
    </head>
    <body>
        <%@include file="Resources/nav.jsp" %>
        <br><br><br><br>

        <div class="container " id="slide-top">

            <div class="row justify-content-around">
                <div class="col-4">
                    <jsp:useBean id="now" class="java.util.Date"/>
                    <fmt:formatDate var="yearnow" value="${now}" pattern="yyyy"/>
                    <fmt:formatDate var="monthnow" value="${now}" pattern="MM"/>
                    <fmt:formatDate var="daynow" value="${now}" pattern="dd"/>
                    <%--<a href="UserMediaServlet"--%>
                    <a href="ArticleMaker?title=&content=&publish_time=${yearnow+1}-${monthnow}-${daynow}&newArticle=true"
                       class="btn btn-outline-secondary  btn-lg" style="text-decoration: none ;">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true" style="padding-right: 10px"></span>
                        <b style="font-family: 'Rajdhani', sans-serif;">CREATE A NEW ARTICLE</b>
                    </a>

                </div>
                <div class="col-4">
                    <a href="SearchResults.jsp"
                       class="btn btn-outline-secondary  btn-lg">
                        <span class="glyphicon glyphicon-zoom-in" aria-hidden="true" style="padding-right: 10px"></span>
                        <b style="font-family: 'Rajdhani', sans-serif;">ADVANCED SEARCH</b>
                    </a>
                </div>
            </div>

            <br> <br>

            <%--Article Cards--%>
            <core:forEach items="${articles}" var="articles">

                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body">
                            <%--Article title links to article--%>
                        <a href="SingleArticleServlet?articleID=${articles.articleID}" class="text-dark"
                           id="hp_article_title1">
                            <h5 class="card-title" style="font-family: 'Julius Sans One', sans-serif;">
                                <b>${articles.title}</b>
                            </h5>
                        </a>

                        <h6 id="hp_author1" style="font-family: 'Titillium Web', sans-serif;">${articles.user_id}</h6>
                        <p class="card-text" id="hp_article_content1"
                           style="font-family: 'Titillium Web', sans-serif;">${articles.publish_time}</p>
                            <%--View Article Button--%>
                        <a href="SingleArticleServlet?articleID=${articles.articleID}"
                           class="btn btn-outline-secondary  btn-sm"
                           style="font-family: 'Rajdhani', sans-serif; text-decoration: none">
                            <span class="glyphicon glyphicon-eye-open" aria-hidden="true"
                                  style="padding-right: 10px"></span>
                            VIEW ARTICLE
                        </a>
                            <%--Edit Article Button--%>
                        <a href="UserMediaServlet?articleID=${articles.articleID}"
                           class="btn btn-outline-secondary  btn-sm"
                           style="font-family: 'Rajdhani', sans-serif; text-decoration: none">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"
                                  style="padding-right: 10px"></span>
                            EDIT
                        </a>
                            <%--Delete Article Button--%>
                        <core:if test="${user!=null && (user.user_id == articles.user_id)}">
                            <a href="ArticleMaker?delArticleID=${articles.articleID}&user_id=${user.user_id}"
                               class="btn btn-outline-secondary  btn-sm"
                               style="font-family: 'Rajdhani', sans-serif; text-decoration: none">
                                <span class="glyphicon glyphicon-trash" aria-hidden="true"
                                      style="padding-right: 10px"></span>
                                DELETE
                            </a>
                        </core:if>
                    </div>
                </div>
                <br>
            </core:forEach>

        </div>
        <hr>
    </body>
</html>
