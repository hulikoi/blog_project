<%--
  Created by IntelliJ IDEA.
  User: nj49
  Date: 4/02/2019
  Time: 3:56 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
              integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
              crossorigin="anonymous">
        <%--CSS external stylesheet--%>
        <link rel="stylesheet" type="text/css" href="Resources/BlogCSS.css">
        <%--Title (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Julius+Sans+One" rel="stylesheet">
        <%--Date (font)--%>
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">

        <title>Search Page</title>
    </head>

    <body>
        <%@include file="Resources/nav.jsp" %>
        <br><br><br><br>
        <%--Search by Title/Author--%>
        <div class="container">
            <form class="form-group" action="ADSearch" method="post">
                <input name="ADsearchString" type="search" placeholder="Search By Title/Author" aria-label="Search"
                       style="outline:none;background-color: transparent;width: 100%;height: 40px;border-radius: 10px;">
                <br><br>
                <%--Date search field--%>
                <input type="date" id="DatesearchString" name="Date1">-
                <input type="date" id="DatesearchString" name="Date2">
                <br><br>
                <%--Search Button--%>
                <div class="justify-content-center">
                    <button class="btn btn-outline-secondary  btn-md" type="submit" id="searchButton2"
                            name="searchButton"
                            value="Search">
                        <span class="glyphicon glyphicon-search" aria-hidden="true" style="padding-right: 10px"></span>
                        SEARCH
                    </button>
                </div>
            </form>
            <br><br>

            <%--Search Headers--%>
            <h1 style="font-family: 'Julius Sans One', sans-serif;">RESULTS</h1>
            <table class="table table-hover" id="slide-top">
                <thead>
                    <tr>
                        <th scope="col">
                            <a href="./Sort?ADSearch=ADSearch&SortColumn=title"
                               style="font-family: 'Julius Sans One', sans-serif; text-decoration: none;color: black">
                                Title
                            </a>
                        </th>
                        <th scope="col">
                            <a href="./Sort?ADSearch=ADSearch&SortColumn=user_id"
                               style="font-family: 'Titillium Web', sans-serif; text-decoration: none;color: black">
                                @username
                            </a>
                        </th>
                        <th scope="col">
                            <a href="./Sort?ADSearch=ADSearch&SortColumn=date"
                                           style="font-family: 'Titillium Web', sans-serif; text-decoration: none;color: black">
                                Publish Date
                            </a>
                        </th>
                    </tr>
                </thead>

                <%--Search Results--%>
                <tbody>
                    <core:forEach items="${Searchedarticles}" var="articles">
                        <tr>
                            <td>
                                <a href="SingleArticleServlet?articleID=${articles.articleID}" class="text-dark"
                                   id="article_title"
                                   style="font-family: 'Julius Sans One', sans-serif; text-decoration: none;color: black">${articles.title}
                                </a>
                            </td>
                            <td style="font-family: 'Titillium Web', sans-serif; text-decoration: none;color: black">
                                @${articles.user_id}</td>
                            <td style="font-family: 'Titillium Web', sans-serif;">${articles.publish_time}</td>
                        </tr>
                    </core:forEach>

                </tbody>
            </table>
        </div>
    </body>
</html>
