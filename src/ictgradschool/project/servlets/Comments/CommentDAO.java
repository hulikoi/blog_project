package ictgradschool.project.servlets.Comments;

import ictgradschool.project.servlets.Accounts.User;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class CommentDAO {
    private Properties dbProps;
    private ServletContext context;

    public CommentDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    public void addComment(Comment comment, User user) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_comment(comment, made_by,articel_id,comment_to) VALUES (?,?,?,?)")) {
                Stmt.setString(1, comment.getComment());
                Stmt.setString(2, user.getuser_id());
                Stmt.setInt(3, comment.getAtricle_id());
                if (comment.getComment_to() == 0) {
                    Stmt.setObject(4, null);
                } else {
                    Stmt.setInt(4, comment.getComment_to());
                }
                Stmt.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean canDeleteComment(int commentID, int articleID, String user_id) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT DISTINCT user_id,made_by FROM group_project_article AS articles,group_project_comment AS comments WHERE comments.comment_id = ? AND comments.articel_id = articles.articel_id")) {
                Stmt.setInt(1, commentID);
                try (ResultSet rs = Stmt.executeQuery()) {
                    int commentuser_idCol = rs.findColumn("made_by");
                    int articleuser_idCol = rs.findColumn("user_id");
                    while (rs.next()) {
                        if (rs.getString(commentuser_idCol).equals(user_id) || (rs.getString(articleuser_idCol).equals(user_id))) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean delComment(int commentID, int articleID, String user_id) {
        if (canDeleteComment(commentID, articleID, user_id)) {
            try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
                try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_comment WHERE comment_id = ?;")) {
                    Stmt.setInt(1, commentID);
                    Stmt.execute();
                    return true;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            return false;
        }
        return false;
    }


    public ArrayList<Comment> getCommentByArticleID(int articleID) {
        Comment comment = null;
        ArrayList<Comment> commentList = new ArrayList<>();
        ArrayList<Comment> parentCommentList = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            //get comment info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT DISTINCT comments.comment_id,comments.comment,comments.made_by,comments.comment_time, comments.comment_to,comments.articel_id,commentUser.profile_pic_url,commentUser.user_id FROM group_project_comment AS comments, group_project_user_db AS commentUser WHERE comments.articel_id = ? AND commentUser.user_id = comments.made_by")) {
                Stmt.setInt(1, articleID);
                try (ResultSet rs = Stmt.executeQuery()) {
                    int commentIDCol = rs.findColumn("comment_id");
                    int userCol = rs.findColumn("made_by");
                    int commentCol = rs.findColumn("comment");
                    int commentToCol = rs.findColumn("comment_to");
                    int commentTimeCol = rs.findColumn("comment_time");
                    int profilePicCol = rs.findColumn("profile_pic_url");
                    while (rs.next()) {
                        comment = new Comment();
                        comment.setCommentID(rs.getInt(commentIDCol));
                        comment.setComment(rs.getString(commentCol));
                        comment.setComment_BY(rs.getString(userCol));
                        comment.setComment_to(rs.getInt(commentToCol));
                        comment.setComment_time(rs.getTimestamp(commentTimeCol));
                        comment.setUserpic_url(rs.getString(profilePicCol));
                        commentList.add(comment);
                    }

                }

            }
            //get parent comment info if exists
            try(PreparedStatement Stmt2 = conn.prepareStatement("SELECT parent.made_by AS parent_made_by,parent.comment_time AS parent_comment_time,reply.comment_id AS reply_comment_id FROM group_project_comment AS parent, group_project_comment AS reply WHERE reply.articel_id= ? AND parent.comment_id = reply.comment_to;")){
                Stmt2.setInt(1, articleID);

                try (ResultSet rs = Stmt2.executeQuery()) {
                    int commentIDCol = rs.findColumn("reply_comment_id");
                    int parentUserCol = rs.findColumn("parent_made_by");
                    int parentCommentTimeCol = rs.findColumn("parent_comment_time");
                    while (rs.next()) {
                        comment = new Comment();
                        comment.setCommentID(rs.getInt(commentIDCol));
                        comment.setReply_to(rs.getString(parentUserCol)+"-"+rs.getTimestamp(parentCommentTimeCol));

                        parentCommentList.add(comment);
                    }

                }

            }

            //add missing info to comments
            for (Comment replycomment:commentList
                 ) {
                for (Comment parent:parentCommentList
                     ) {
                    if(replycomment.getCommentID()==parent.getCommentID()){
                        replycomment.setReply_to(parent.getReply_to());
                    }
                }
            }

            return commentList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return commentList;
    }
}
