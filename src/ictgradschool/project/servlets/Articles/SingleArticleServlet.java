package ictgradschool.project.servlets.Articles;

import ictgradschool.project.servlets.Comments.Comment;
import ictgradschool.project.servlets.Comments.CommentDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;


@WebServlet(name = "SingleArticleServlet")
public class SingleArticleServlet extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int articleID = Integer.parseInt(request.getParameter("articleID"));
        HttpSession session = request.getSession();
        ArticleDAO dao = new ArticleDAO(getServletContext());
        CommentDAO dao2 = new CommentDAO(getServletContext());
        Article article = dao.getArticleByID(articleID);
        request.setAttribute("article", article);
        ArrayList<Comment> comment = dao2.getCommentByArticleID(articleID);
        request.setAttribute("comment", comment);
        request.setAttribute("articleID", articleID);
        request.getRequestDispatcher("SingleArticleView.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}






