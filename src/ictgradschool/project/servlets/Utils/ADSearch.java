package ictgradschool.project.servlets.Utils;

import ictgradschool.project.servlets.Articles.Article;
import ictgradschool.project.servlets.Articles.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

public class ADSearch extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HttpSession session = request.getSession();
        ArticleDAO dao3 = new ArticleDAO(getServletContext());
        ArrayList<Article> Searchedarticles;

        if ((request.getParameter("Date1") == null || request.getParameter("Date2") == null) || (request.getParameter("Date1").length() == 0 || request.getParameter("Date2").length() == 0)) {
            Searchedarticles = dao3.getADSearchedArticles(request.getParameter("ADsearchString"));
        } else {
            Searchedarticles = dao3.getDateSearchedArticles(request.getParameter("ADsearchString"), request.getParameter("Date1"), request.getParameter("Date2"));
        }

        request.setAttribute("Searchedarticles", Searchedarticles);
        session.setAttribute("Searchedarticles", Searchedarticles);
        request.getRequestDispatcher("SearchResults.jsp").forward(request, response);
    }


}
