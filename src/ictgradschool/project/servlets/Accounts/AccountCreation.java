package ictgradschool.project.servlets.Accounts;

import ictgradschool.project.servlets.Articles.Article;
import ictgradschool.project.servlets.Articles.ArticleDAO;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class AccountCreation extends HttpServlet {
    //for del user
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //if deluser_id parameter not null then delete user,else get user profile pics,
        // else set up the default picture list for the user creation.
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if (request.getParameter("deluser_id") != null) {
            String user_id = request.getParameter("deluser_id");

            UserDAO dao = new UserDAO(getServletContext());
            dao.delUser(user_id);
            session.invalidate();
            request.getRequestDispatcher("HomePageSetup").forward(request, response);
        } else if (user != null) {
            List<String> picurls = user.getPics();
            request.setAttribute("pics", picurls);
            ArticleDAO dao = new ArticleDAO(getServletContext());
            List<Article> articles = dao.getUpcomingArticles(user);
            List<Article> articleVotes = dao.getLastTenUserArticles(user);
            request.setAttribute("articleVotes", articleVotes);
            request.setAttribute("articles", articles);
            request.getRequestDispatcher("UserPage.jsp").forward(request, response);
        } else {
            List<String> picurls = Arrays.asList("Photos/DEFAULT_1.jpg", "Photos/DEFAULT_2.jpg", "Photos/DEFAULT_3.jpg");
            request.setAttribute("pics", picurls);
            request.getRequestDispatcher("UserPage.jsp").forward(request, response);
        }


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //get values from form
        String user_id = request.getParameter("user_id");
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String country = request.getParameter("country");
        String bio = request.getParameter("bio");
        String picurl = request.getParameter("picurl");
        String dobString = request.getParameter("dob");
        //added list for default pics
        List<String> picurls = Arrays.asList("Photos/DEFAULT_1.jpg", "Photos/DEFAULT_2.jpg", "Photos/DEFAULT_3.jpg");

        byte[] salt = Passwords.getNextSalt();
        int iteration = (int) (Math.random() * 100000) + 1000000;
        //convert date to sql format
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date javaDob = null;
        try {
            javaDob = format.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        java.sql.Date sqlDob = new java.sql.Date(javaDob.getTime());

        //create a bse64encoded version of salted,hashed,randomly iterated password
        String pass = Passwords.base64Encode(Passwords.hash(request.getParameter("password").toCharArray(), salt, iteration));
        //create new user object with these details. Add to db
        User newUser = new User(user_id, lname, fname, country, picurl, pass, bio, salt, iteration, sqlDob, picurls);
        UserDAO dao = new UserDAO(request.getServletContext());
        dao.addUser(newUser);

        //setup User media folder
        File uploadfolder = new File(getServletContext().getRealPath("Photos/MediaUploads/" + user_id));
        if (!uploadfolder.exists()) {
            uploadfolder.mkdir();
        }

        HttpSession session = request.getSession();
        session.setAttribute("user", newUser);
        request.setAttribute("user", newUser);

        //get the user's profile pictures and future articles
        picurls = newUser.getPics();
        request.setAttribute("pics", picurls);
        ArticleDAO dao2 = new ArticleDAO(getServletContext());
        List<Article> articles = dao2.getUpcomingArticles(newUser);
        request.setAttribute("articles", articles);
        request.getRequestDispatcher("UserPage.jsp").forward(request, response);
    }
}
