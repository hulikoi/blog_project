package ictgradschool.project.servlets.Accounts;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;


public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession session = request.getSession(); // this will create a session if one doesn't exist.
        if (request.getParameter("logout") != null) {
            session.invalidate();
            response.sendRedirect("HomePageSetup");
            return;
        }

        //connect to db and retrieve user data if exists
        UserDAO dbConection = new UserDAO(getServletContext());
        User user = dbConection.getUser(request.getParameter("user_id"));

        //check if returned user is null ie user not found. if so send back to homepage.
        //if exists, hash supplied password and compare, if match them goto article creation pg else say user_id / password don't match.
        if (user == null) {
            session.setAttribute("user", user);
            request.setAttribute("user", user);
//            response.sendRedirect("HomePageSetup");
            request.getRequestDispatcher("HomePageSetup").forward(request, response);
        } else {
            //adding password checking stuff to local variables
            String loginPassword = request.getParameter("password");
            char[] testPassword = loginPassword.toCharArray();
            byte[] userByte = user.getSalt();
            byte[] expectedHash = Passwords.base64Decode(user.getPassword());
            int userIterations = user.getIteration();

            //verifying password, sending to article creation pg or booting back to login page
            if (Passwords.isExpectedPassword(testPassword, userByte, userIterations, expectedHash)) {
                session.setAttribute("user", user);
                request.setAttribute("user", user);
                response.sendRedirect("ArticleMaker");
            } else {
                request.getRequestDispatcher("HomePageSetup").forward(request, response);
            }
        }
    }


    //not used, will deal with requests via post to ensure confidentiality of log in numbers
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        doPost(request, response);
    }
}
