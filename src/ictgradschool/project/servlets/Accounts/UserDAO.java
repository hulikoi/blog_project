package ictgradschool.project.servlets.Accounts;

import javax.servlet.ServletContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class UserDAO {
    private Properties dbProps;
    private ServletContext context;

    public UserDAO(ServletContext context) {
        this.context = context;
        dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream(context.getRealPath("WEB-INF/dbProps.properties"))) {//
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Class.forName("com.mysql.jdbc.Driver"); // com.mysql.jdbc.Driver
        } catch (Exception except) {
            except.printStackTrace();
        }
    }

    //create a user object from the db info and return that user to calling method.
    public User getUser(String user_id) {
        User user = null;
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT * FROM group_project_user_db WHERE user_id = ?")) {
                Stmt.setString(1, user_id);

                try (ResultSet rs = Stmt.executeQuery()) {
                    List<String> picurls = getPicList(user_id);
                    int pwCol = rs.findColumn("passwords");
                    int fnameCol = rs.findColumn("user_fname");
                    int lnameCol = rs.findColumn("user_lname");
                    int countryCol = rs.findColumn("country");
                    int dateCol = rs.findColumn("date_of_birth");
                    int bioCol = rs.findColumn("bio");
                    int pic_urlCol = rs.findColumn("profile_pic_url");
                    int iteraCol = rs.findColumn("iterations");
                    int saltCol = rs.findColumn("salt");

                    String passwords = null, user_fname = null, user_lname = null, country = null, bio = null, profile_pic_url = null;
                    java.sql.Date date_of_birth = null;
                    Integer iteration = 0;
                    byte[] salt = null;
                    while (rs.next()) {
                        passwords = rs.getString(pwCol);
                        user_fname = rs.getString(fnameCol);
                        user_lname = rs.getString(lnameCol);
                        country = rs.getString(countryCol);
                        bio = rs.getString(bioCol);
                        profile_pic_url = rs.getString(pic_urlCol);
                        date_of_birth = rs.getDate(dateCol);
                        iteration = rs.getInt(iteraCol);
                        salt = rs.getBytes(saltCol);
                        user = new User(user_id, user_lname, user_fname, country, profile_pic_url, passwords, bio, salt, iteration, date_of_birth, picurls);
                    }
                    return user;
                }
            }
        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
        return user;
    }

    public void addUser(User user) {
        // try to add new user
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_user_db (user_id, passwords, user_fname, user_lname, country,date_of_birth, bio, profile_pic_url, salt, iterations) VALUES (?,?,?,?,?,?,?,?,?,?)")) {
                Stmt.setString(1, user.getuser_id());
                Stmt.setString(2, user.getPassword());
                Stmt.setString(3, user.getFname());
                Stmt.setString(4, user.getLname());
                Stmt.setString(5, user.getCountry());
                Stmt.setDate(6, user.getDob());
                Stmt.setString(7, user.getBio());
                Stmt.setString(8, user.getUrl());
                Stmt.setBytes(9, user.getSalt());
                Stmt.setInt(10, user.getIteration());
                Stmt.execute();
            }
            for (String url : user.getPics()
            ) {
                try (PreparedStatement Stmt2 = conn.prepareStatement("INSERT INTO group_project_userpics(user_id, url) VALUES (?,?)")) {
                    Stmt2.setString(1, user.getuser_id());
                    Stmt2.setString(2, url);
                    Stmt2.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void addPicURL(User user) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            //prepare the query to get the user info
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET profile_pic_url =? WHERE user_id = ?")) {
                Stmt.setString(1, user.getUrl());
                Stmt.setString(2, user.getuser_id());
                Stmt.execute();
            }
            //todo can probably integrate this is earlier statement.
            try (PreparedStatement Stmt = conn.prepareStatement("INSERT INTO group_project_userpics(user_id, url) VALUES (?,?)")) {
                Stmt.setString(1, user.getuser_id());
                Stmt.setString(2, user.getUrl());
                //execute update
                Stmt.execute();
            }

        } catch (SQLException e) { //catch for the connection try
            e.printStackTrace();
        }
    }

    public void addPicurltolist(User user) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_userpics SET url =?,user_id = ?")) {
                Stmt.setString(1, user.getUrl());
                Stmt.setString(2, user.getuser_id());
                Stmt.execute();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> getPicList(String user_id) {
        List<String> urlList = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT url FROM group_project_userpics WHERE user_id = ?")) {
                Stmt.setString(1, user_id);
                try (ResultSet rs = Stmt.executeQuery()) {
                    while (rs.next()) {
                        urlList.add(rs.getString(1));
                    }
                    Collections.sort(urlList);
                    return urlList;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collections.sort(urlList);
        return urlList;
    }

    //modify a user's details.
    protected void modifyUser(User user) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            if (user.getPassword() == null) {
                try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET user_fname=?,user_lname=?,country=?,profile_pic_url=?,bio=?,date_of_birth=? WHERE user_id = ?")) {
                    Stmt.setString(1, user.getFname());
                    Stmt.setString(2, user.getLname());
                    Stmt.setString(3, user.getCountry());
                    Stmt.setString(4, user.getUrl());
                    Stmt.setString(5, user.getBio());
                    Stmt.setDate(6, user.getDob());
                    Stmt.setString(7, user.getuser_id());
                    Stmt.execute();
                }
            } else {
                try (PreparedStatement Stmt = conn.prepareStatement("UPDATE group_project_user_db SET user_fname=?,user_lname=?,country=?,profile_pic_url=?,bio=?,date_of_birth=?,passwords=?,salt=?,iterations=? WHERE user_id = ?")) {
                    Stmt.setString(1, user.getFname());
                    Stmt.setString(2, user.getLname());
                    Stmt.setString(3, user.getCountry());
                    Stmt.setString(4, user.getUrl());
                    Stmt.setString(5, user.getBio());
                    Stmt.setDate(6, user.getDob());
                    Stmt.setString(7, user.getPassword());
                    Stmt.setBytes(8, user.getSalt());
                    Stmt.setInt(9, user.getIteration());
                    Stmt.setString(10, user.getuser_id());
                    Stmt.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void delUser(String user_id) {
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("DELETE FROM group_project_user_db WHERE user_id = ?;")) {
                Stmt.setString(1, user_id);
                Stmt.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public List<String> getuser_ids() {
        List<String> user_ids = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            try (PreparedStatement Stmt = conn.prepareStatement("SELECT user_id FROM group_project_user_db")) {
                try (ResultSet rs = Stmt.executeQuery()) {
                    while (rs.next()) {
                        user_ids.add(rs.getString(1));
                    }
                    Collections.sort(user_ids);
                    return user_ids;
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        Collections.sort(user_ids);
        return user_ids;
    }
}
