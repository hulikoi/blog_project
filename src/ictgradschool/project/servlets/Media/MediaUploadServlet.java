package ictgradschool.project.servlets.Media;

import ictgradschool.project.servlets.Accounts.User;
import ictgradschool.project.servlets.Articles.Article;
import ictgradschool.project.servlets.Articles.ArticleDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "MediaUploadServlet")
@MultipartConfig
public class MediaUploadServlet extends HttpServlet {

    private File uploadfolder, tempfolder;

    @Override
    public void init() throws ServletException {
        super.init();
        //check upload folder exists, if not create it
        this.uploadfolder = new File(getServletContext().getRealPath("/Photos/MediaUploads"));
        if (!uploadfolder.exists()) {
            uploadfolder.mkdir();
        }

        //create temp upload folder
        this.tempfolder = new File(getServletContext().getRealPath("/WEB-INF/temp"));
        if (!tempfolder.exists()) {
            tempfolder.mkdir();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        PrintWriter out = response.getWriter();

        ArticleDAO articleDAO = new ArticleDAO(getServletContext());
        Article article = null;

        DiskFileItemFactory factory = new DiskFileItemFactory();
        factory.setSizeThreshold(4 * 1024);
        factory.setRepository(tempfolder);
        ServletFileUpload upload = new ServletFileUpload(factory);

        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        uploadfolder = new File(String.valueOf(uploadfolder) + "/" + user.getuser_id());
        if (!uploadfolder.exists()) {
            uploadfolder.mkdir();
        }
        response.setContentType("text/html");

        if (user == null) {
            out.println("oops no user");
        }

        try {
            List<FileItem> fileItems = upload.parseRequest(request);
            File fullsizeMediaFile = null;

            for (FileItem fi : fileItems
            ) {
                if (fi.isFormField()) {
                    if (fi.getFieldName().equals("articleID")) {
                        article = articleDAO.getArticleByID(Integer.parseInt(fi.getString()));
                    }
                }
                if (!fi.isFormField()) {
                    String filename = fi.getName();
                    fullsizeMediaFile = new File(uploadfolder + "/" + filename);// change to userID folder
                    fi.write(fullsizeMediaFile);
                }
            }
            File photosFolder = new File(String.valueOf(uploadfolder));
            File[] files = photosFolder.listFiles();
            List<String> mediaList = new ArrayList<>();

            for (File file : files
            ) {
                mediaList.add(".\\Photos/MediaUploads" + "\\" + user.getuser_id() + "\\" + file.getName());
            }

            request.setAttribute("mediaList", mediaList);
            request.setAttribute("article", article);
            request.getRequestDispatcher("EditArticle.jsp").forward(request, response);
            uploadfolder = new File(getServletContext().getRealPath("/Photos/MediaUploads"));
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}



